# EP Laravel Initializer

To make our development faster, we have created EP Laravel Initializer package. This package will install all the packages that we usually use for all the projects.

#### Default Packages

EP Laravel Initializer includes the following packages.

| Category      | Name          |
| ------------- | ------------- |
| Multi-tenancy  | TenancyforLaravel  |
| Modularizer  | Laravel CA Modules |
| Authentication  | Passport  |
| Social Integration  | Socialite |
| AWS services  | AWS SDK  |
| Cache  | Predis |
| Auditing  | Laravel Auditing  |
| Search  | Algolia Scout Extended |
| Debugger  | Laravel Telescope  |
| Payment  | Laravel Cashier |
| Roles & Permission  | Spatie  |
| Slugs  | Spatie Sluggable |
| Mail Testing | Laravel Mailable Test|
| Image Processor  | InterventionLaravolt Avatar |
| Error Monitoring  | Laravel Sentry |
| Coding Standard  | Laravel PHP CS Fixer  |
| 2FA | Google2FA Laravel |
| Recovery  | Recovery Codes |
| Mail Editor  | Laravel Mail Editor (Aka MailEclipse) |
| Country List  | [Country List](https://github.com/Monarobase/country-list) |
| Laravel Enum  | [Laravel Enum](https://github.com/BenSampo/laravel-enum) |
| Laravel Repository  | [Laravel 5 Repositories](https://github.com/andersao/l5-repository) |
| Export  | [Laravel Excel](https://github.com/SpartnerNL/Laravel-Excel) |

#### Composer

Execute the following command to get the latest version of the package:

```
 composer require escapepixel/laravel-initializer
```


#### Install default packages (All)

Execute the following command to install default packages

```
php artisan initializer:start
```

#### Configure default packages (All)

Once all the packages are installed, execute the following command to configure packages.

```
php artisan initializer:configure
```


#### Install specific packages 

If you wish to install specific packages, execute the following command to install.

```
php initializer:install {name}
```

#### Configure specific packages 

```
php artisan initializer:configure-package
```
