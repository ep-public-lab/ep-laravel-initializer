<?php

namespace EP\LaravelInitializer\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class ConfigureDefaultPackages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'initializer:configure';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Configure default packages';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $names = Cache::get("default");
        $extraSteps = config('extra-steps');
        $predefinedNames = config('initializer');

        if ($names === NULL) return $this->error("There is no package to configure");

        foreach ($names as $name) {
            if (!array_key_exists($name, $extraSteps)) {
                continue;
            }
            $predefinedNames[$name];

            $steps = $extraSteps[$name];
            foreach ($steps as $step) {
                if (isset($step['options'])) {
                    $this->call($step['command'], $step['options']);
                } else {
                    $this->call($step['command']);
                }
            }
        }

        $this->info('All Packages Configured Successfully');
        $names = Cache::forget("default");
    }
}
