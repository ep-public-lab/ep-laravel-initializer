<?php

namespace EP\LaravelInitializer\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class InstallDefaultPackages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'initializer:start {names?*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install default packages';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $args = $this->arguments();

        $predefinedNames = config('initializer');
        $names = sizeof($args['names']) > 0 ? $args['names'] : array_keys($predefinedNames);
       
        $packageLists=[];
        $this->comment('These pacakges will be installed...');
        
        foreach($names as $name) {
            if (array_key_exists($name, $predefinedNames)) {
                $packagearr[0] = $name;
                $packagearr[1] = $predefinedNames[$name]; 
                array_push($packageLists, $packagearr);
            }
        }
        $this->table(
            ['Name', 'Package'],
            $packageLists
        );
        $bar = $this->output->createProgressBar(count($names));
        $bar->start();
        $this->newLine();
        foreach($names as $name) {
            if (!array_key_exists($name, $predefinedNames)) {
                $this->error("$name doesn't exist. Please specify the correct package name");
                continue;
            }

            $package = $predefinedNames[$name];
            $path = base_path();
            exec("cd {$path} && composer require {$package}");
            $this->info("Installed Successfully - {$package}");
            $bar->advance();
            $this->newLine();
        }
        $bar->finish();
       
        $this->info("All Packages Installed Successfully");
        Cache::put("default", $names);
    }
}
