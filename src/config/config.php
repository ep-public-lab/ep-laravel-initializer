<?php

return [
    "modularizer"           => "escapepixel/laravel-ca-modules",
    "tenancy"               => "stancl/tenancy",
    "slug"                  => "spatie/laravel-sluggable",
    "role-permission"       => "spatie/laravel-permission",
    "social-integration"    => "laravel/socialite",
    "debugger"              => "laravel/telescope",
    "authentication"        => "laravel/passport",
    "cache"                 => "predis/predis",
    "search"                => "algolia/scout-extended:^2.0",
    "payment"               => "laravel/cashier",
    "image-processor"       => "intervention/image",
    "error-monitoring"      => "sentry/sentry-laravel",
    "coding-standard"       => "stechstudio/laravel-php-cs-fixer",
    "mail-testing"          => "spatie/laravel-mailable-test",
    "aws"                   => "aws/aws-sdk-php",
    "auditing"              => "owen-it/laravel-auditing",
    "avatar"                => "laravolt/avatar",
    "2fa"                   => "pragmarx/google2fa-laravel",
    "recovery"              => "pragmarx/recovery",
    "mail-editor"           => "qoraiche/laravel-mail-editor",
    "country-list"          => "monarobase/country-list",
    "laravel-enum"          => "bensampo/laravel-enum",
    "repository"            => "prettus/l5-repository",
    "laravel-excel"         => "psr/simple-cache:^2.0 maatwebsite/excel" // If composer require fails on Laravel 9
];
