<?php

return [
    "debugger" => [
        ['command' => 'telescope:install'],
        ['command' => 'migrate']
    ],
    "role-permission" => [
        ['command' => 'vendor:publish', 'options' => ['--provider' => 'Spatie\Permission\PermissionServiceProvider']],
        ['command' => 'config:clear'],
        ['command' => 'migrate']
    ],
    "authentication" => [
        ['command' => 'migrate'],
        ['command' => 'passport:install']
    ],
    "queue" => [
        ['command' => 'horizon:install']
    ],
    "search" => [
        ['command' => 'vendor:publish', 'options' => ['--provider' => 'Laravel\Scout\ScoutServiceProvider']]
    ],
    "payment" => [
        ['command' => 'migrate'],
        ['command' => 'vendor:publish', 'options' => ['--tag' => 'cashier-migrations']]
    ],
    "tenancy"  => [
        ['command' => 'tenancy:install']
    ],
    "coding-standard" => [
        ['command' => 'vendor:publish', 'options' => ['--provider' => 'STS\Fixer\FixerServiceProvider']]
    ],
    "modularizer"   => [
        ['command' => 'vendor:publish', 'options' => ['--tag' => 'config']]
    ],
    "2fa"  => [
        ['command' => 'vendor:publish', 'options' => ['--provider' => 'PragmaRX\Google2FALaravel\ServiceProvider']]
    ],
    "mail-editor" => [
        ['command' => 'laravel-mail-editor:install']
    ],
    "repository" => [
        [
            'command' => 'vendor:publish',
            'options' => [
                '--provider' => 'rettus\Repository\Providers\RepositoryServiceProvider'
            ]
        ]
    ],
    "laravel-excel" => [
        [
            'command' => 'vendor:publish',
            'options' => [
                '--provider' => 'Maatwebsite\Excel\ExcelServiceProvider',
                '--tag' => 'config'
            ]
        ]
    ]
];
